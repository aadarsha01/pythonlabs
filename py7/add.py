def calculate_discount(price, discount_rate):
    discount_amount = price * (discount_rate / 100)
    discounted_price = price - discount_amount
    return discount_amount, discounted_price

def main():
    try:
        price = float(input("Enter the original price: $"))
        discount_rate = float(input("Enter the discount rate (%): "))
        
        if price < 0 or discount_rate < 0:
            print("Please enter positive values for price and discount rate.")
            return
        
        discount_amount, discounted_price = calculate_discount(price, discount_rate)
        
        print("Original Price: ${:.2f}".format(price))
        print("Discount Rate: {:.2f}%".format(discount_rate))
        print("Discount Amount: ${:.2f}".format(discount_amount))
        print("Discounted Price: ${:.2f}".format(discounted_price))
    except ValueError:
        print("Invalid input. Please enter numeric values for price and discount rate.")

if __name__ == "__main__":
    main()
