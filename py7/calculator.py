def add(numbers):
    return sum(numbers)

def subtract(numbers):
    result = numbers[0]
    for num in numbers[1:]:
        result -= num
    return result

def multiply(numbers):
    result = 1
    for num in numbers:
        result *= num
    return result

def divide(numbers):
    result = numbers[0]
    for num in numbers[1:]:
        if num == 0:
            return "Error: Division by zero!"
        result /= num
    return result

def calculator():
    print("Select operation:")
    print("1. Add")
    print("2. Subtract")
    print("3. Multiply")
    print("4. Divide")

    choice = input("Enter choice (1/2/3/4): ")

    if choice in ('1', '2', '3', '4'):
        numbers = []
        while True:
            num = input("Enter a number (or 'done' to finish): ")
            if num.lower() == 'done':
                break
            try:
                numbers.append(float(num))
            except ValueError:
                print("Invalid input. Please enter a valid number.")

        if choice == '1':
            print("Result:", add(numbers))
        elif choice == '2':
            print("Result:", subtract(numbers))
        elif choice == '3':
            print("Result:", multiply(numbers))
        elif choice == '4':
            print("Result:", divide(numbers))
    else:
        print("Invalid input")

if __name__ == "__main__":
    while True:
        calculator()
        next_calculation = input("Do you want to perform another calculation? (yes/no): ")
        if next_calculation.lower() != 'yes':
            break
