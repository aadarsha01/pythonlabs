def create_file(filename, content):
    try:
        with open(filename, 'w') as file:
            file.write(content)
        print(f"File '{filename}' has been created.")
    except Exception as e:
        print(f"An error occurred while creating the file: {str(e)}")

def read_and_display_content(filename):
    try:
        with open(filename, 'r') as file:
            print("File content:")
            print(file.read())
    except FileNotFoundError:
        print(f"Error: File '{filename}' does not exist.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")

def main():
    filename = input("Enter the filename: ")
    content = input("Enter the content to write into the file (type 'EOF' on a new line to finish): ")
    while True:
        line = input()
        if line == 'EOF':
            break
        content += '\n' + line
    create_file(filename, content)
    read_and_display_content(filename)

if __name__ == "__main__":
    main()
