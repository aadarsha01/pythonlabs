def create_file(filename, content):
    try:
        with open(filename, 'w') as file:
            file.write(content)
        print(f"File '{filename}' has been created.")
    except Exception as e:
        print(f"An error occurred while creating the file: {str(e)}")

def main():
    filename = input("Enter the filename: ")
    print("Enter the content to write into the file. Type 'stop' on a new line to finish.")
    content = ""
    while True:
        line = input()
        if line == 'stop':
            break
        content += line + '\n'
    create_file(filename, content)

if __name__ == "__main__":
    main()
